# officeMap

#### 介绍
查找办公室位置  
![img.png](img.png)

#### 软件架构
软件架构说明:样式库使用的是bootstrap，js库使用的是jquery  
该项目主要是使用了canvas来画的图，jq操作dom，和使用了bootstrap的一些组件  
加上打包工具webpack的简单配置，处理跨域和使用一些插件

#### 安装教程

1. npm  install
2. npm run dev
3. 在dev环境下，使用的webpack的代理处理跨域可以正常访问
4. build后的dist目录只有放在服务器上才能运行，因为有跨域问题

#### 使用说明

1. npm run dev

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


