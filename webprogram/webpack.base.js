const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
module.exports = {

  entry:{
    index:"./src/index.js"
  },
  output:{
    filename:'scripts/[name].[chunkhash:5].js',
    publicPath:'/'
  },
  plugins:[
    new HtmlWebpackPlugin({
      template:'./public/index.html',
      filename: 'index.html',
      thunk:['index']
    }),
    new CopyWebpackPlugin([
      {from:'./public',to:'./'}
    ]),
    new MiniCssExtractPlugin({
      // 打包 css 代码 到文件中
      filename: "css/[name].css",
      chunkFilename: "css/common.[hash:5].css", // 针对公共样式的文件名
    }),
  ],
  module: {
    rules: [
      {test: /\.(png|jpe?g|gif)$/i, loader: 'file-loader',options: { outputPath: 'images',name: '[path][name].[ext]'}},
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
    ]
  }
}
