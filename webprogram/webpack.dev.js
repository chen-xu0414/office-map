const webpack = require("webpack")
module.exports = {
  mode:'development',
  devtool:'source-map',
  devServer: {
    open: true,
    openPage:'index.html',
    proxy: {
      // 配置多个跨域
      '/%E5%AD%A6%E9%99%A2%E4%BF%A1%E6%81%AF%E6%9F%A5%E8%AF%A2': {
        target: 'http://81.70.77.89:8080/',
        changeOrigin:true,
      }
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      URL:JSON.stringify('')
    })
  ]
}
