const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const webpack = require('webpack')

module.exports = {
  mode:'production',
  plugins:[
    new CleanWebpackPlugin(),
    new webpack.DefinePlugin({
      URL:JSON.stringify(`http://81.70.77.89:8080`)
    })
  ]
}
