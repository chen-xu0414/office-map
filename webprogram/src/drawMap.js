import deepBlue from '../assets/辅导员deepBlue.png'
import lv from '../assets/辅导员lv.png'
import park from '../assets/辅导员park.png'
import orange from '../assets/辅导员orange.png'
import loutiImg from '../assets/楼梯 (2).png'
import girlWc from '../assets/女厕所 (1).png'
import manWc from '../assets/男厕所 (1).png'
import electron from '../assets/微电子.png'
import computerImg1 from '../assets/物理图_计算机.png'
import computerImg2 from '../assets/台式机，计算机，电脑.png'
import information from '../assets/信息.png'
export const can = document.getElementById("can")
const ctx = can.getContext('2d')
export const positionArr = [

]
let colors = ["rgba(242,99,149,0.5)", "rgba(98,239,171,0.5)", "rgba(255,232,104,0.5)", "rgba(128,227,247,0.5)", "rgba(215,129,249,0.5)", "rgba(0,123,255,0.5)", "transparent"];
let colors2 = ["rgba(242,99,149,1)", "rgba(98,239,171,1)", "rgba(255,232,104,1)", "rgba(128,227,247,1)", "rgba(215,129,249,1)", "rgba(0,123,255,1)", "transparent"];

function alignTest(str, begX, begY) {
  ctx.font = '18px bold Arial'
  ctx.fillStyle = '#fdfdfd'
  let arr = str.slice('')
  for (let i = 0; i < arr.length; i++) {
    if (i === 1) {
      ctx.fillText('|', begX + 2, begY + (i * 15))
    } else {
      ctx.fillText(arr[i], begX, begY + (i * 15 + 1))
    }
  }
  ctx.beginPath()
  ctx.strokeStyle = 'black'
}

function drawImage (image , x  , y , alpha)
{
  // 绘制图片
  ctx.drawImage(image , x , y);
  // 获取从x、y开始，宽为image.width、高为image.height的图片数据
  // 也就是获取绘制的图片数据
  var imgData = ctx.getImageData(x , y , image.width , image.height);
  for (var i = 0 , len = imgData.data.length ; i < len ; i += 4 )
  {
    // 改变每个像素的透明度
    imgData.data[i + 3] = imgData.data[i + 3] * alpha;
  }
  // 将获取的图片数据放回去。
  ctx.putImageData(imgData , x , y);

}

function fillText(str, begX, begY) {
  ctx.font = '18px Sans bold'
  ctx.fillStyle = '#fdfdfd'
  ctx.fillText(str, begX, begY)
  ctx.beginPath()
  ctx.strokeStyle = 'black'
}
function fillText2(str, begX, begY) {
  ctx.font = '14px Sans'
  ctx.fillStyle = '#fdfdfd'
  ctx.fillText(str, begX, begY)
  ctx.beginPath()
  ctx.strokeStyle = 'black'
}

ctx.strokeRectPro = function(name,begX,begY,w,h,color){
  positionArr.push({
    name,
    begX,
    begY,
    endX:begX+w,
    endY:begY+h
  })
  ctx.shadowBlur=10;
  ctx.shadowColor='#999';
  ctx.shadowOffsetX = 15
  ctx.shadowOffsetY = 15





  ctx.strokeRect(begX,begY,w,h)


  ctx.fillStyle = color
  ctx.fillRect(begX,begY,w,h)
}
let dpr = window.devicePixelRatio; // 假设dpr为2
// 获取css的宽高
let { width: cssWidth, height: cssHeight } = can.getBoundingClientRect();
// 根据dpr，扩大canvas画布的像素，使1个canvas像素和1个物理像素相等
can.style.width = can.width + 'px';
can.style.height = can.height + 'px';

can.width = dpr * cssWidth;
can.height = dpr * cssHeight;
// 由于画布扩大，canvas的坐标系也跟着扩大，如果按照原先的坐标系绘图内容会缩小
// 所以需要将绘制比例放大
ctx.scale(dpr,dpr);
// **************************
ctx.strokeRectPro("3-211",0, 0, 65, 50,colors[0])
fillText('3-211', 11, 32)
ctx.clearRect(58,49,7,2)


ctx.strokeRectPro("3-212",0, 50, 50, 60,colors[1])
fillText2('3-212', 8, 82)
ctx.clearRect(49,93,2,10)
ctx.clearRect(49,56,2,10)

ctx.strokeRectPro("3-213",0, 110, 50, 50,colors[3])
fillText2('3-213', 8, 140)
ctx.clearRect(49,140,2,15)

ctx.strokeRectPro("3-210",65, 0, 70, 75,colors[5])
fillText('3-210', 77, 45)

ctx.strokeRectPro("3-209",65, 75, 70, 60,colors[4])
fillText('3-209', 77, 112)
ctx.clearRect(64,95,2,20)

ctx.strokeRectPro("3-208",135, 0, 30, 135,colors[3])
alignTest('3-208', 147, 40)
ctx.clearRect(141,134,10,2)

ctx.strokeRectPro("3-207",165, 0, 30, 135,colors[2])
alignTest('3-207', 177, 40)
ctx.clearRect(180,134,10,2)

ctx.strokeRectPro("3-206",195, 0, 30, 135,colors[1])
alignTest('3-206', 207, 40)
ctx.clearRect(200,134,10,2)

ctx.strokeRectPro("3-205",225, 0, 30, 135,colors[4])
alignTest('3-205', 236, 40)
ctx.clearRect(240,134,10,2)


ctx.strokeRectPro("3-204",255, 0, 45, 75,colors[5])
// fillText('3-204', 258, 40)
fillText2('3-204', 259, 40)
ctx.clearRect(256,74,15,2)


ctx.strokeRectPro("3-203",300, 0, 45, 105,colors[1])
fillText2('3-203', 306, 50)


ctx.clearRect(256,74,15,2)


ctx.clearRect(299,90,2,20)


ctx.strokeRectPro("3-202",285, 105, 60, 52,colors[3])
fillText('3-202', 293, 126)

ctx.strokeRectPro("3-201",285, 157, 60, 52,colors[1])
// 楼梯
const fudaoyuan = new Image()
const fudaoyuanlv = new Image()
const fudaoyuanPark = new Image()
const fudaoyuanOrange = new Image()
fudaoyuan.src=deepBlue
fudaoyuanlv.src=lv
fudaoyuanPark.src=park
fudaoyuanOrange.src=orange
fudaoyuanlv.onload = function (){
  ctx.drawImage(fudaoyuanlv,303,182,28,28)
}
fudaoyuanPark.onload = function (){
  ctx.drawImage(fudaoyuanPark,303,282,28,28)

}
fudaoyuanOrange.onload = function (){
  ctx.drawImage(fudaoyuanOrange,303,335,28,28)

}
fudaoyuan.onload = function (){
  ctx.drawImage(fudaoyuan,303,130,28,28)

  ctx.drawImage(fudaoyuan,302,446,32,32)
}
fillText('3-201', 293, 178)

ctx.clearRect(284,111,2,20)
ctx.clearRect(284,182,2,20)
// 楼梯
const louti = new Image()
louti.src=loutiImg
louti.onload = function (){
  ctx.drawImage(louti,283,202,64,64);
}


ctx.strokeRectPro("4-201",285, 260, 60, 53,colors[4])
fillText('4-201', 293, 280)
ctx.strokeRectPro("4-202",285, 313,60,52,colors[2])
fillText('4-202', 293, 333)


ctx.clearRect(284,265,2,20)
ctx.clearRect(284,338,2,20)

// 中间广场
ctx.strokeRectPro("",0, 160, 255, 170,colors[6])
ctx.strokeRectPro("",0, 165, 250, 160,colors[6])
ctx.clearRect(0, 166,6,158)

//厕所
ctx.strokeRectPro("",285, 365, 60, 35,colors[1])
// fillText('厕所(女)', 290, 388)
const img = new Image()
img.src=girlWc
img.onload = function (){
  ctx.drawImage(img,296,362,40,40);
}
ctx.clearRect(284,375,2,10)

ctx.strokeRectPro("4-203",285, 400, 60, 105,colors[3])
fillText('4-203', 293, 438)
ctx.clearRect(284,409,2,20)
ctx.clearRect(284,477,2,20)


ctx.strokeRectPro("4-204",285, 505, 60, 105,colors[4])
fillText('4-204', 293, 564)
ctx.clearRect(284,514,2,20)
ctx.clearRect(284,582,2,20)

//厕所
ctx.strokeRectPro("",285, 610, 60, 35,colors[1])
const img2 = new Image()
img2.src=manWc
img2.onload = function (){
  ctx.drawImage(img2,296,606,40,40);
}
ctx.clearRect(284,620,2,10)


ctx.strokeRectPro("4-205",285, 645, 60, 35,colors[5])
fillText('4-205', 293, 670)
ctx.clearRect(284,655,2,10)

ctx.strokeRectPro("4-206",285, 680, 60, 35,colors[2])
fillText('4-206', 293, 704)
ctx.clearRect(284,690,2,10)

// 大办公区
//左侧
ctx.strokeRectPro("4-207",0, 365, 120, 90,colors[1])
fillText('4-207', 36, 398)
const dianzi = new Image()
dianzi.src=electron
dianzi.onload = function (){
  ctx.drawImage(dianzi,40,405,40,40);
}
ctx.clearRect(119,426,2,20)

ctx.strokeRectPro("4-208",0, 455, 120, 100,colors[3])
fillText('4-208', 36, 513)
ctx.clearRect(119,500,2,20)

ctx.strokeRectPro("4-209",0, 555, 120, 90,colors[4])
fillText('4-209', 36, 584)
const computer = new Image()
const computer2 = new Image()
computer.src=computerImg1
computer2.src=computerImg2
computer.onload = function (){
  ctx.drawImage(computer,40,594,40,40);
}
computer2.onload = function (){
  ctx.drawImage(computer2,190,568,40,40);
}
ctx.clearRect(119,565,2,20)
// 右侧
ctx.strokeRectPro("4-210",160, 365, 95, 140,colors[5])
fillText('4-210', 186, 425)
const xinxi = new Image()
xinxi.src=information
xinxi.onload = function (){
  ctx.drawImage(xinxi,190,433,40,40);
}
ctx.clearRect(159,474,2,20)

ctx.strokeRectPro("4-211",160, 505, 95, 140,colors[1])
fillText('4-211', 186, 557)
ctx.clearRect(159,518,2,20)



// can.ontouch = function(e){
//   console.log(e)

// }






