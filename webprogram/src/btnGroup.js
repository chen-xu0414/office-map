import $ from 'jquery'
$('#findBtn').hide()
$('#codeBtn').hide()
$('#mapBtn').hide()
let show = false
$('#moreBtn').click(function (){
  if(!show){
    $('#findBtn').show().css('top','-50px').css('opacity','1')
    $('#codeBtn').show().css('top','-100px').css('opacity','1')
    $('#mapBtn').show().css('top','-150px').css('opacity','1')
    show = true
  }else {
    $('#findBtn').css('top','0px').css('opacity','0.5')
    $('#codeBtn').css('top','0px').css('opacity','0.5')
    $('#mapBtn').css('top','0px').css('opacity','0.5')
      setTimeout(function (){
        $('#findBtn').hide()
        $('#codeBtn'). hide()
        $('#mapBtn'). hide()
      },450)
    show = false
  }
}
)
