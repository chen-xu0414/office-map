import $ from 'jquery'
import imgSrc from "../assets/qrcode.png"
import getDataByname from "./util/getDataByname"
export default async function (){
  let val = $('#inputName').val()
  const resAllTeacher = await getDataByname(val)
  if(!resAllTeacher[0].姓名){
    val = ''
  }
  if(val !== ''){
    const res = await fetch(URL+'/学院信息查询/二维码/' + val)
    return res.url
  }else {
    return imgSrc
  }

}
export function handleImg(src) {
  let str = `<img src="${src}" alt="" width="200px" height="200px"/>`
  $('#codeDetail').html(str).css('text-align', 'center')
  $('#codeShow').modal('show')
  $('[data-toggle="popover"]').popover('hide')
  $('#downLoad').attr('href',src).click(function (){

  })
}
