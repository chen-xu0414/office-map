import { positionArr, can } from "./drawMap"
import $ from "jquery"
import getDataByRoom from "./util/getDataByroom"
import getDataByName from "./util/getDataByname"
import createImg, { handleImg } from "./createImg"

export function bindEvent() {
  can.onclick = function (e) {
    const touchX = e.layerX
    const touchY = e.layerY
    const res = positionArr.find(item => {
      if (item.name) {
        if (touchX > item.begX && touchX < item.endX && touchY > item.begY && touchY < item.endY)
          return true
      }
      return false
    })
    if (res) {
      $('#teachersInfo').html('')
      $('#boxShow').modal('show')
      $('#staticBackdropLabel').text(res.name)
      getDataByRoom(res.name).then(resp => {
        let str = ' <div class="accordion mt-2" id="accordionExample">'
        if (resp.办公室名) {
          $('#staticBackdropLabel').text(`${res.name} ${resp.办公室名}`)
          $('#tableHead').show()
          resp.教师信息.forEach(item => {
            let insert = {}
            if(typeof item.时间 === 'string'){
              insert.color = 'text-info'
              insert.str = '<div>\n' +
                '    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-circle" viewBox="0 0 16 16">\n' +
                '  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>\n' +
                '  <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>\n' +
                '</svg>\n' +
                '    <span style="font-size: 14px">未知</span>\n' +
                '  </div>'
            }else if(!item.时间){
              insert.str = '<div>\n' +
                '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-circle-fill" viewBox="0 0 16 16">\n' +
                '<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>\n' +
                '</svg>\n' +
                '<span style="font-size: 14px">没课</span>\n' +
                '</div>\n'
              insert.color = 'text-success'
            }else if(item.时间){
              insert.str = '<div>\n' +
                ' <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-fill" viewBox="0 0 16 16">\n' +
                '<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>\n' +
                '</svg>\n' +
                '<span style="font-size: 14px">有课</span>\n' +
                '</div>'
              insert.color = 'text-secondary'
            }
            str += `
                <div class="card">
                  <div class="card-header" id="heading${item.姓名}">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse${item.姓名}" aria-expanded="true" aria-controls="collapseOne">
                        ${item.姓名}
                        <span class="float-right iconfont icon-jiantou_liebiaozhankai"></span>
                      </button>
                    </h2>
                  </div>

                  <div id="collapse${item.姓名}" class="collapse " aria-labelledby="heading${item.姓名}" data-parent="#accordionExample">
<!--               跟换的位置   -->
  <div class="card-body ${insert.color}">
  <div class=" d-flex justify-content-between"> 职位：${item.职务.join('、')} 
  
  ${insert.str}

 </div>
        ${item.备注?`<hr/><div>备注：${item.备注}</di
>`:'<div></div>'}
           
 </div>
                  </div>
                </div>
`
          })
        } else {
          str += "该办公室目前还没有老师。"
          $('#tableHead').hide()
        }
        str += '</div>'
        $('#teachersInfo').html(str)
      })
      $('[data-toggle="popover"]').popover('hide')
    }

  }
// 两个模态框的事件
  $('#boxShow').on('hidden.bs.modal', function () {
    $('[data-toggle="popover"]').popover('show')
    $('.btns button:first').show()

  }).on('show.bs.modal', function () {
    $('.btns button').hide()
    $('.btns a').hide()
  })

  $('#codeShow').on('hidden.bs.modal', function () {
    $('[data-toggle="popover"]').popover('show')
    $('.btns button:first').show()
  }).on('show.bs.modal', function () {
    $('.btns button').hide()
    $('.btns a').hide()
  })
  $('#inputName').on('input', function () {
    const close = $(this).next()
    close.show()
  })

  $('#reset').click(function () {
    const input = $(this).prev()
    input.val('')
    $(this).hide()
    $('#searchBtn').click()
  })

  $('#searchBtn').click(function () {
    // 隐藏 上一次的结果
    $('[data-toggle="popover"]').popover('hide')
    $('.finded').remove()
    // 保存hash
    const data = $('#inputName').val()
    if (data) {
      location.hash = '#' + data
    }
    // 处理重叠
    const positionContainer = []
    const dirArr = ['top', 'bottom', 'left', 'right']
    let num = 1
    data && getDataByName(data).then(resp => {
      resp.forEach(item => {
        const findData = positionArr.find(iten => {
          return iten.name === item.办公室
        })
        if (findData) {
          const h = findData.endY - findData.begY
          const w = findData.endX - findData.begX
          const x = findData.begX + w / 2
          const y = findData.begY + h / 2
          const name = findData.name
          let dir = dirArr[0]
          // if(roomContainer.includes(name)){
          // }else {
          //   roomContainer.push(findData.name)
          // }
          const res = positionContainer.find(item => item.x === x && item.y === y)
          if (res) {
            let n = (res.dir + 1) % 4
            dir = dirArr[n]
          } else {
            positionContainer.forEach(item => {
              if (Math.abs(item.x - x) <= 108 && Math.abs(item.y - y) <= 20) {
                dir = dirArr[num]
                num++
                num %= 4
              }
            })
            const dirNum = dirArr.indexOf(dir)
            positionContainer.push({x, y, dir: dirNum})
          }
          if (item.办公室 === '4-205' && positionContainer.length > 1) {
            dir = 'left'
          }
          $(`<span class="iconfont icon-zuobiao finded" data-container="body" data-toggle="popover" 
              data-placement=${dir}
              data-content="${item.姓名} ${item.职务[0]}  ${item.办公室}">
            </span>`)
            .css('left', findData.begX + w / 2)
            .css('top', findData.begY + h / 2)
            .appendTo($('#mapBox'))
          setTimeout(function () {
            $('[data-toggle="popover"]').popover('show')
          }, 1000)
        }
      })
    })
  })

  $('#codeBtn').click(function () {
    createImg().then(resp => {
      handleImg(resp)
    })
  })

}

$(function () {
  $('[data-toggle="popover"]').popover()
})
