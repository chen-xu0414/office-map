const webpackDev = require("./webpack.dev")
const webpackBase = require("./webpack.base")
const webpackProd = require("./webpack.prod")
module.exports = function (env){
  if (env && env.prod){
    const webpackFin = {
      ...webpackBase,...webpackProd
    }
    webpackFin.plugins = [...webpackProd.plugins,...webpackBase.plugins]
    return webpackFin
  }else{
    const webpackFin = {
      ...webpackBase,...webpackDev
    }
    webpackFin.plugins = [...webpackBase.plugins,...webpackDev.plugins]
    return webpackFin
  }
}
